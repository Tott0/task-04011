package com.example.laboratorio.myapplication;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;

public class AddEntryDialog extends DialogFragment {

    public interface AddEntryDialogListener{
        public void onDialogPositiveClick(DialogFragment dialog, String key, String val);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    AddEntryDialogListener mListener;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);

        try{
            mListener = (AddEntryDialogListener) activity;
        }
        catch (ClassCastException e){
            throw new ClassCastException(activity.toString()
                    + "must implement AddEntryDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.add_entry_dialog_fragment,null))
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                        String key = ((EditText) getDialog().findViewById(R.id.etKey)).getText().toString();
                        String val = ((EditText) getDialog().findViewById(R.id.etVal)).getText().toString();
                        mListener.onDialogPositiveClick(AddEntryDialog.this, key, val);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AddEntryDialog.this.getDialog().cancel();
                        mListener.onDialogNegativeClick(AddEntryDialog.this);
                    }
                });
        return builder.create();
    }
}
