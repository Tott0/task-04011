package com.example.laboratorio.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by Totto on 11/04/2016.
 */
public class ValueDialogFragment extends DialogFragment{
    String value;

    static ValueDialogFragment newInstance(String val){
        ValueDialogFragment f = new ValueDialogFragment();

        //Supply num input as argument
        Bundle args = new Bundle();
        args.putString("val",val);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstaceState){
        super.onCreate(savedInstaceState);
        value = getArguments().getString("val");
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Value of selected Key = " + value)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
