package com.example.laboratorio.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LongClickDialog extends DialogFragment {

    String key;

    public interface LongClickDialogListener{
        public void onLongDialogPositiveClick(DialogFragment dialog);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    LongClickDialogListener mListener;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);

        try{
            mListener = (LongClickDialogListener) activity;
        }
        catch (ClassCastException e){
            throw new ClassCastException(activity.toString()
                    + "must implement OptionDialogListener");
        }
    }

    static LongClickDialog newInstance(String key){
        LongClickDialog f = new LongClickDialog();

        //Supply num input as argument
        Bundle args = new Bundle();
        args.putString("key",key);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstaceState){
        super.onCreate(savedInstaceState);
        key = getArguments().getString("key");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Selected field market by key: " + key)
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onLongDialogPositiveClick(LongClickDialog.this);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });;
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
