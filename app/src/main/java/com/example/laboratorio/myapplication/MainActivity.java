package com.example.laboratorio.myapplication;

import android.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements
        AddEntryDialog.AddEntryDialogListener, OptionDialog.OptionDialogListener,
        LongClickDialog.LongClickDialogListener, ViewAdapter.RecyclerClickListner{


    Firebase myFirebaseRef;
    ArrayList<String> list = new ArrayList<String>();

    ArrayAdapter<String> adapter;
    ListView mLv;


    private List<Information> mData = new ArrayList<>();
    private RecyclerView mrV;
    private ViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;



    private final String oRoot = "https://task-0411.firebaseio.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Firebase.setAndroidContext(this);
        myFirebaseRef = new Firebase(oRoot);
        myFirebaseRef.orderByKey();

        mLv = (ListView)findViewById(R.id.listView);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        mLv.setAdapter(adapter);

        mrV = (RecyclerView) findViewById(R.id.recycle);
        mrV.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mrV.setLayoutManager(mLayoutManager);
        mAdapter = new ViewAdapter(this,mData);
        mrV.setAdapter(mAdapter);

        mAdapter.setRecyclerClickListner(this);


        setListener();

        mLv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = adapter.getItem(position).toString();
                String ref = myFirebaseRef.getRef().toString() + "/" + item;
                ref = ref.replaceAll("\\s+","");

                myFirebaseRef = new Firebase(ref);
                setListener();


            }
        });
        mLv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String item = adapter.getItem(position).toString();
                item = item.replaceAll("\\s+","");
                LongClickDialog longclick = LongClickDialog.newInstance(item);
                longclick.show(getFragmentManager(),"longclick_dialog");
                return false;
            }
        });

    }

    private void setListener(){
        myFirebaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    String value = snapshot.getValue().toString().replace("{", "");
                    value = value.replace("}", "");


                    if (snapshot.getChildrenCount() > 0)
                    {
                        TextView tv = (TextView) findViewById(R.id.textView);
                        String title = "";
                        if(snapshot.getKey() == null){
                            title = myFirebaseRef.getRoot().toString();
                            title = title.replaceAll("https:\\/\\/","");
                        }
                        else{
                            title = snapshot.getKey();
                        }
                        tv.setText(title);
                        adapter.clear();
                        mAdapter.clearData();

                        for (String v : value.split(",")) {
                            adapter.add(v.split("=")[0]);
                            Information info = new Information(v.split("=")[0]);
                            mAdapter.addItem(mData.size(), info);
                        }





                    }
                    else
                    {
                        String item = snapshot.getValue().toString();
                        ValueDialogFragment dia = ValueDialogFragment.newInstance(item);
                        dia.show(getFragmentManager(), "value_dialog");
                        onClickbtnBack(null);
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this,"Error inesperado",Toast.LENGTH_SHORT).show();
                    onClickbtnBack(null);
                }

            }

            @Override
            public void onCancelled(FirebaseError error) {
            }
        });
    }

    public void onClickbtnOption(View view) {
        OptionDialog option = new OptionDialog();
        option.show(getFragmentManager(), "option_dialog");
    }
    public void onClickbtnAddEntry(View view) {
        AddEntryDialog addEntry = new AddEntryDialog();
        addEntry.show(getFragmentManager(),"addEntry_dialog");
    }
    public void onClickbtnBack(View view) {
        String ref = myFirebaseRef.getRef().toString();
        ref = ref.replaceAll("(?<!:\\/)\\/[^\\/]*(?=\\z)","");
        myFirebaseRef = new Firebase(ref);
        setListener();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String key, String val) {

        myFirebaseRef.child(key).setValue(val);
        setListener();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String db) {
        String oRef = myFirebaseRef.getRoot().toString();
        String ref = "https://" + db + ".firebaseio.com/";
        myFirebaseRef = new Firebase(ref);
        setListener();
    }

    @Override
    public void onLongDialogPositiveClick(DialogFragment dialog) {
        LongClickDialog longclick = (LongClickDialog) dialog;
        myFirebaseRef.child(longclick.key).removeValue();
        setListener();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }


    @Override
    public void itemClick(View view, int position) {

        Log.d("asd", mAdapter.getItem(position).title);
        String item = mAdapter.getItem(position).title;
        String ref = myFirebaseRef.getRef().toString() + "/" + item;
        ref = ref.replaceAll("\\s+","");

        myFirebaseRef = new Firebase(ref);
        setListener();
    }
}
