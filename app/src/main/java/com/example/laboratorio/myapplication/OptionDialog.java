package com.example.laboratorio.myapplication;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;

public class OptionDialog extends DialogFragment {

    public interface OptionDialogListener{
        public void onDialogPositiveClick(DialogFragment dialog, String db);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    OptionDialogListener mListener;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);

        try{
            mListener = (OptionDialogListener) activity;
        }
        catch (ClassCastException e){
            throw new ClassCastException(activity.toString()
                    + "must implement OptionDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.option_dialog_fragment,null))
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                        String db = ((EditText) getDialog().findViewById(R.id.etDB)).getText().toString();
                        mListener.onDialogPositiveClick(OptionDialog.this, db);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        OptionDialog.this.getDialog().cancel();
                        mListener.onDialogNegativeClick(OptionDialog.this);
                    }
                });
        return builder.create();
    }
}
